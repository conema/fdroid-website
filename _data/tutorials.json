{
  "misc": {
    "step_x": "Step [number]",
    "step_x_description": "Step [number]: [description]",
    "option_x_description": "Option [number]: [description]",
    "overview": {
      "title": "What You Can Do with F-Droid",
      "summary": "F-Droid is an independent, community-sourced app store for Android that is completely free and open source software. Within F-Droid you can browse over 1,200 open source apps, search and install apps from created repositories, or create your own repository. Open f-droid.org on your Android phone to download the app and get started!",
      "tutorial_x_title": "Tutorial [number]: [title]"
    }
  },
  "add_repo": {
    "overview": {
      "title": "Get more content.",
      "summary": "To get more, add a repo to F-Droid. A repo is a collection of curated content. You can find out about people’s repos from them directly. Developers often link to their F-Droid repo from their website. Trainers often share links to repos over email, Facebook or Twitter."
    },
    "title": "How to Add a Repo to F-Droid",
    "intro": "You can add a repo to F-Droid by scanning the repo's QR code from another device or by opening a repo link on your phone. Begin by downloading and installing the F-Droid app on your Android phone.",
    "open_repo_link": {
      "short_summary": "Open the Repo Link",
      "title": "Open the Repo Link on Your Phone",
      "description": "Someone has sent you a link to their repo via email, Facebook, Twitter or text. You want to add these apps to your F-Droid app. You can easily add a repo to F-Droid on your Android phone by opening its link.",
      "steps": {
        "step_1": "Download and install the F-Droid app on your Android.",
        "step_2": "Click on the link and open it in a web browser on your phone.",
        "step_3": "When you have the link open, tap the 'Add to F-Droid' button.",
        "step_4": "Tap the 'I have F-Droid button. The F-Droid app will open.",
        "step_5": "The 'Add new repository' view will be shown with the repository address prefilled. Tap 'Ok' to add it.",
        "step_6": "Your new repo will be added. To see a list of your repos, open 'Settings'. Select 'Repositories'.",
        "step_7": "Tap on a repo to see its contents.",
        "step_8": "Select the apps you'd like to have and install them on your phone."
      }
    },
    "scan_qr_code": {
      "short_summary": "Scan QR Code",
      "title": "Scan a QR Code from Another Device",
      "description": "If someone near you has a repo you want, you can scan the repo QR code from their phone. Or, if a QR code for a repo is displayed on a website, scan it on your phone to add it.",
      "steps": {
        "step_1": "Once the QR code for the repo is displayed, scan the QR code using a QR scanner app.",
        "step_2": "Tap the link. If given the option, open the link using F-Droid. Then follow prompts. If this works, you are done! If not, proceed to the next step.",
        "step_3": "If you do not get the option to open the link with F-Droid, close the QR scanner app and open the F-Droid app.",
        "step_4": "Within F-Droid, tap 'Settings'. Under 'My Apps' select 'Repositories'.",
        "step_5": "On the Repositories screen, tap the '+' icon in the top corner.",
        "step_6": "The repo address will be auto-populated. Tap 'Enable'. Note: The repo will be saved in F-Droid Repositories, but no apps will be installed until you manually choose and install."
      },
      "related_tutorial": "You may also be interested in: <a href=\"../swap/\">How to Send and Receive Apps Offline</a>"
    },
    "no_internet": "No internet? No problem. Learn <a href=\"../swap/\">How to Send and Receive Apps Offline.</a>"
  },
  "swap": {
    "overview": {
      "title": "Send and receive apps offline.",
      "summary": "No internet? No problem. F-Droid Nearby gives you the ability to send and receive content to people in the same room."
    },
    "title": "How to Send and Receive Apps Offline",
    "intro": "No internet? No problem. Download apps from people near you.",
    "all_devices_need_fdroid": "All devices need F-Droid downloaded and installed before beginning. All devices should follow the steps below.",
    "step_1": "Open F-Droid. Tap 'Nearby' from the menu at the bottom of your screen.",
    "step_2": "Tap on 'Find people near me'. The app will automatically begin searching.",
    "step_3": "Once people nearby are found, both you and the contact must select each other.",
    "step_4": "Once connected, choose the apps you wish to share with your contact.",
    "step_5": "Tap the '—>' button.",
    "step_6": "One person will be prompted to confirm the request to connect.",
    "step_7": "Tap the 'Install' button next to the apps you want to install.",
    "step_8": "Tap 'Install' once more. Then, follow the Android prompts to complete the installation process.",
    "step_9": "Once installs are complete, your apps are available in your Android app gallery and in your F-Droid app under installed apps.",
    "step_10": "To access your installed apps, open F-Droid settings and select 'Manage installed apps' under 'My Apps.'",
    "related_tutorial": "You may also be interested in: <a href=\"../create-repo/\">How to Create a Repo</a>"
  },
  "create_repo": {
    "overview": {
      "title": "Add your own apps and files.",
      "summary": "Create your own customized repository of apps. Use F-Droid to distribute content. Trainers use repos to easily share a collection of resources and apps with participants at a training when there’s no reliable internet connection. Developers use repos to distribute their apps to niche audiences."
    },
    "title": "How to Create a Repo",
    "related_tutorial": "You may also be interested in: <a href='../add-repo/'>How to Add a Repo to F-Droid</a>",
    "intro": "Can't find what you're looking for? Why not create your own customized collection of apps and files using Repomaker. Download the Repomaker web app on your computer to get started.",
    "download_and_install": {
      "title": "Download and Install Repomaker to Start Creating",
      "steps": {
        "step_1": "Download and install the Repomaker web app to your desktop computer.",
        "step_2": "Launch the Repomaker web app. Login or Signup to begin.",
        "step_3": "Create a new repo and name it accordingly.",
        "step_4": "Choose to add apps from the gallery and available repositories, or upload your own files. Click the 'Add from Gallery' button to choose from readily available content.",
        "step_5": "Click the 'Add' button next to the items you wish to add. When you're finished, click the 'back' arrow to return to your repo.",
        "step_6": "Your apps will be displayed in your repo content view."
      }
    },
    "customize_repo": {
      "title": "Customize Your Repo",
      "description": "You are able to edit the app and file summaries to make it understandable for your target audience. You can edit the language making it country specific. Then share the repo seamlessly to attendees prior to conferences and trainings.",
      "steps": {
        "step_1": "Click on an item to open its details.",
        "step_2": "Click 'Edit'.",
        "step_3": "Customize the content to fit your target audience. Note: The changes you make will be made to your repo only. They will not update for the original source. Please use respect when customizing the information for content that is not yours.",
        "step_4": "All of your changes are auto-saved."
      }
    },
    "share_repo": {
      "title": "Customize Your Repo",
      "steps": {
        "step_1": "With the repo you wish to share open, click 'Share' in the menu. Note: You need to set up a place to store your repo before sharing. Instructions for how to do this are in Step 2.",
        "step_2": "You will be prompted to set up storage in order to publish your repo and before sharing. If you do not see this page, continue to Step 4.",
        "step_3": "Choose the storage option that best fits your needs. Once you have storage, you're ready to share! Return to 'Share'.",
        "step_4": "Choose to copy the link and send it directly to people through email or text, or share your repo on Facebook or Twitter. When people open the link to your repo, they will need to add it to an Android device to access the apps in the repo. See <a href='../add-repo/'>How to Add a Repo to F-Droid</a> for more information.",
        "step_5": "The 'View Repo' option allows you to see what you've just published or shared."
      }
    },
    "update_repo": {
      "title": "Update Your Repo",
      "steps": {
        "step_1": "If you add apps from the gallery, they are automatically updated when the source adds new versions. For files that you uploaded, you can drag and drop updated versions in the repo view.",
        "step_2": "Alternatively, you can open a single item to add a new version. Select edit.",
        "step_3": "Scroll down to versions. Then, add a new version under the 'Version History' section.",
        "step_4": "Then, add a new version under the 'Version History' section."
      }
    }
  }
}
